
public class PrimzahlSuche {
	
	//langesame suche
	public void primzahlen(int N) {

		boolean[] a = new boolean[N];
		for (int i = 0; i < N; i++) {
			a[i] = true;
		}
		for (int i = 2; i < N; i++) {

			for (int j = 2; j < N; j++) {
				if ((i % j == 0) && (j != i))
					a[i] = false;
			}
		}
	}

	public void schnellePrimzahl(int N) {
		boolean[] a = new boolean[N];
		for (int i = 0; i < N; i++) {
			a[i] = true;
		}
		for (int i = 3; i < N; i += 2) {
			for (int j = 3; j < Math.sqrt(i); j += 2) {
				if ((i % j == 0)) {
					a[i] = false;
					break;
				}
			}
		}
	}

	// Sieb
	public void sieb(int N) {
		boolean[] a = new boolean[N];
		for (int i = 0; i < N; i++) {
			a[i] = true;
		}
		for (int i = 2; i < Math.sqrt(N); i++) {
			if (a[i] == true)
				for (int j = 2; i * j < N; j++) {
					a[i * j] = false;
				}
		}
	}
}
